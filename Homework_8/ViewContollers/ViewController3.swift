//
//  ViewController3.swift
//  Homework_8
//
//  Created by Andrew Kvasha on 25.07.2022.
//

import UIKit

class ViewController3: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    @IBAction func didPressedBack(_ backButton: UIButton) {
        
        navigationController?.popViewController(animated: true)
    }
}
