//
//  ViewController2.swift
//  Homework_8
//
//  Created by Andrew Kvasha on 25.07.2022.
//

import UIKit

class ViewController2: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    deinit {
        print("Deallocated")
    }
    
    @IBAction func didPressedBack(_ backButton: UIButton) {
        
        dismiss(animated: true)
    }
}
